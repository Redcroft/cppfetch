![](screenshot.png)

# cppFetch

cppFetch is a command line system information tool written in c++  
Based on the python GoFetch application  
by Katelyn Hamer: https://gitlab.com/KatHamer/gofetch

## Dependencies

I've tried to keep dependencies to a minimum, but i am using cmake (learning to use cmake)
although it isn't strictly required.

* `gcc > 4.8.1` or `clang > 3.3`
* `cmake > 2.8`

## Installation

clone repository

```sh
git clone https://gitlab.com/Redcroft/cppfetch.git cppFetch
```

cd to the build directory and run cmake

```sh
cd cppFetch/build
cmake ..
```

Once complete, compile and install

```sh
make
sudo make install
```

finally run cppFetch

```sh
cppFetch
```
