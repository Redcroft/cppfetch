#include <iostream>
#include <string>
#include <map>
#include <fstream>
#include <iterator>
#include <vector>
#include <cstring>
#include <cerrno>
#include <sys/utsname.h>

/*
  Cpp command line system information
  Author: Richard Redcroft https://gitlab.com/Redcroft/cppfetch
  based on GoFetch by Katelyn Hamer: https://gitlab.com/KatHamer/gofetch
*/

// declare functions
std::string get_distro();
std::string color_entry(std::string title,
                        std::string value, std::string accent);
void color_test();

// Color test blocks
std::string blocks = "████";

// Color escape codes
std::map<std::string, std::string> colors = { {"red", "\033[0;31m"},
                                              {"green", "\033[0;32m"},
                                              {"yellow", "\033[0;33m"},
                                              {"blue", "\033[0;34m"},
                                              {"magenta", "\33[0;35m"},
                                              {"cyan", "\033[0;36m"},
                                              {"reset", "\033[0;0m"},
                                              {"bold", "\033[;1m"}};

int main() {
    // initialise utsname struct to get system info
    struct utsname sysinfo;
    uname(&sysinfo);

    // get distroname (throw error if unable to open os-release file)
    std::string distro = get_distro();

    // Output system information
    std::string color = "blue";
    std::cout << color_entry("\nUser", "redsith", color);;
    std::cout << color_entry("OS", sysinfo.release, color);
    std::cout << color_entry("Distro", distro, color);
    std::cout << color_entry("Hostname", sysinfo.nodename, color);
    std::cout << color_entry("Terminal", getenv("TERM"), color);
    std::cout << color_entry("Shell", getenv("SHELL"), color) << "\n";

    // Output color line
    color_test();

    return 0;
}


std::string get_distro(){
    // Get the distro from /etc/os-release, print error and return blank
    // if unable to read file
    std::ifstream readFile;
    std::string distro = "";
    std::string filename = "/etc/os-release";
    readFile.open(filename);

    if (!readFile) {
        std::cout << std::strerror(errno) << filename << "\n\n";
    } else {
        std::string line;

        // loop over all lines in /etc/os-release looking for line
        // with NAME as first element: eg
        // NAME="opensuse Tumbleweed"
        // Then get the substring based on the " positions
        while(std::getline(readFile, line)){
            if(line.find("NAME") == 0){
                size_t pos = line.find_first_of('"');
                size_t end = line.find_last_of('"');
                distro = line.substr(pos+1, (end-pos-1));
                break; //exit loop as we are done
            }
        }
    }
    // return distro name or blank string
    return distro;
}

std::string color_entry(std::string title,
                        std::string value, std::string accent){
    //Format a string with a colored accent
    std::string reset = colors.find("reset")->second;
    std::string color = colors.find(accent)->second;
    std::string col_result = color + title + " " + reset + value + "\n";
    return col_result;
}

void color_test(){
    // print colored blocks to console
    std::string colored_blocks = "";
    // using vector to define color order, as map is sorted by default
    // so unable to loop over its elements
    std::vector<std::string> color_list = { "red", "green", "yellow",
                                            "blue", "magenta", "cyan", "bold"
    };

    // Add each block to colored_blocks
    for(std::string item: color_list){
        colored_blocks.append(colors.find(item)->second + blocks + " ");
    }

    // Finally print the line to console
    std::cout << colored_blocks << colors.find("reset")->second << 
        "\n" << std::endl;
}
